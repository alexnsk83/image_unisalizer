<?php
require_once('../core/bootstrap.php');

spl_autoload_register(function ($controller) {
    include_once ROOT_DIR . str_replace('\\', '/', '/' . $controller . '.php');
});

$params = [];
$p = explode('/', $_SERVER['REQUEST_URI']);
foreach ($p as $v) {
    if ($v !== '') {
        $params[] = $v;
    }
}

if (isset($params[0]) && $params[0] == 'admin') {
    if (!isset($params[1])) {
        $params[0] = 'admin/articles';
    } else {
        $params[1] = $params[0] . '/' . $params[1];
        array_shift($params);
    }
}

//Выбор контроллера
$controllerName = isset($params[0]) ? $params[0] : 'main';
switch ($controllerName) {
    case 'users':
        $controller = new \controllers\Users($params);
        break;
    case 'main':
        $controller = new \controllers\Main($params);
        break;
    case 'converter':
        $controller = new \controllers\Converter($params);
        break;
    default:
        $controller = new \controllers\Page($params);
        break;
}

//Выбор действия
$action = 'action_';
$action .= isset($params[1]) ? $params[1] : 'index';

$controller->$action();
$controller->render();