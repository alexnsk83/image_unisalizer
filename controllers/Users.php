<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.03.2016
 * Time: 18:20
 * Контроллер форм регистрации и авторизации
 */

namespace controllers;

use core\Validator;
use core\Template;

class Users extends Base
{
    protected $mUsers;
    protected $params;
    protected $registerErrors = [];
    protected $regData = [];
    protected $form_correct = true;
    protected $loginErrors = [];
    protected $activateMessage;

    public function __construct()
    {
        parent::__construct();
        $this->mUsers = \models\Users::instance();
    }

    public function action_index()
    {
        redirectTo('/');
    }

    public function action_login()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST" && count($_POST) > 1){
            $this->mUsers = \models\Users::instance();
            $user = $this->mUsers->getSpecified($_POST['email'], md5($_POST['email'] . md5($_POST['password'])));
            if ($user && $user['is_active'] == 0){
                redirectTo('/users/activate');
            }

            if (!empty($user)) {
                $_SESSION['auth'] = true;
                $_SESSION['login'] = $user['email'];
                $_SESSION['user_id'] = $user['id_user'];
                $_SESSION['is_active'] = $user['is_active'];
                if (isset($_POST['remember']) && $_POST['remember'] == 'on') {
                    setcookie('remember', $_POST['remember'], time() + 60 * 60 * 24 * 365, "/");
                    setcookie('key', md5(12345 . md5($_POST['email'] . md5($_POST['password']))), time() + 60 * 60 * 24 * 365, "/");
                } else {
                    setcookie('remember', '');
                }
                redirectTo('/');
            } else {
                $this->loginErrors['wrong'] = "Неправильный логин или пароль";
            }
        }


        $this->pageTitle = "Вход";
        $this->layout = 'layouts/base';
        $this->subTemplate = Template::generate('login',
            [
                'loginErrors' => $this->loginErrors,
            ]
        );
    }

    public function action_registration()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' && count($_POST) > 0){

            $regData = array_map('trim', $_POST);
            $regData = array_map('strip_tags', $regData);
            $this->registerErrors = Validator::extractArray($regData, ['email', 'password', 'password_repeat']);

            $this->form_correct = $this->registerErrors ? false : true;
            if ($this->form_correct){
                $regData['login'] = $regData['email'];
                \models\Users::instance()->add($regData['login'], $regData['email'], md5($regData['email'].md5($regData['password'])));

                $_SESSION['reg_ok_msg'] = "Вы успешно зарегистрировались, письмо для активации отправлено на почту";
                redirectTo('/');
            }
        }

        $this->pageTitle = "Регистрация";
        $this->layout = 'layouts/base';
        $this->subTemplate = Template::generate('register',
            [
                'registerErrors' => $this->registerErrors,
                'regData' => $this->regData,
            ]
        );
    }

    public function action_activate()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET'){
            $uri = explode("?", $_SERVER['REQUEST_URI']);
            $params = explode("&", $uri[1]);
            foreach ($params as $item => $i){
                $exploded = explode("=", $i);
                $get[$exploded[0]] = $exploded[1];
            }
            $activator = \models\Users::instance();
            $result = $activator->activate('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], $get['id']);
            if ($result){
                $this->activateMessage = "Ваша учётная запись успешно активирована, теперь Вы можете войти";
            } else {
                $this->activateMessage = "Произошла ошибка активации";
            }
        }

        $this->pageTitle = "Активация учётной записи";
        $this->layout = 'layouts/base';
        $this->subTemplate = Template::generate('activation',
            [
                'activateMessage' => $this->activateMessage,
            ]
        );
    }

    public function action_users()
    {
        $this->mUsers->truncateUsers();
    }

    public function action_registers()
    {
        $this->mUsers->truncateRegisters();
    }

    //Выход из учётки
    public function action_logout()
    {
        $this->mUsers->logout();
    }
}