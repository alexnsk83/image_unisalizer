<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 17.03.2016
 * Time: 22:17
 * Контроллер построения главной страницы
 */
namespace controllers;

use core\Validator;
use core\Template;
use models\Converter;
use models\Users;

class Main extends Base
{
    protected $imageErrors = [];
    protected $loginErrors = [];

    public function __construct()
    {
        parent::__construct();

    }

    //Отображение главной страницы. Указываем раскладку и внутренний шаблон
    public function action_index()
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST" && count($_POST) > 0){
            //Если нажата кнопка на форме загрузки изображения - обрабатываем и запускаем приложение
            if ($_POST['file_base64'] == null && $_FILES['image']['name'] == null){
                $this->imageErrors = Validator::extractArray($_FILES, ['image']);
            };

            if (!$this->imageErrors){

                //формируем путь файла на сервере
                $user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
                $path = USER_DATA_DIR . "images/" . $user_id . "/" . date('j-m-y') . "/";
                if (!file_exists($path)){
                    mkdir($path);
                }

                //Сохраняем исходное изображение в файл
                $_SESSION['user_id'] = $user_id;
                $_SESSION['path'] = $path;
                if ($_FILES['image']['name'] != null){
                    $file_name = $_SESSION['image'] = $_FILES['image']['name'];
                    move_uploaded_file($_FILES['image']['tmp_name'], $path . $file_name);
                } else {
                    $file_name = $_SESSION['image'] = $_POST['file_name'];
                    $image = USER_DATA_DIR . "images/" . $_SESSION['user_id'] . "/" . date('j-m-y') . "/" . $file_name;
                    $data = explode(',', $_POST['file_base64']);
                    $encodedData = str_replace(' ', '+', $data[1]);
                    $decodedData = base64_decode($encodedData);

                    file_put_contents($image, $decodedData);
                }


                redirectTo("/converter");
            }
        }

        $this->layout = 'layouts/base';

        $this->subTemplate = Template::generate('index',
            [
                'imageErrors' => $this->imageErrors,
                'loginErrors' => $this->loginErrors,
            ]
        );
    }

    public function render()
    {
        $this->pageTitle = "Главная";
        $this->js = [
            "jquery-2.2.2.js",
            "main.js",
            "imageprocess.js",
            "dragdrop.js"
        ];
        parent::render();
    }
}