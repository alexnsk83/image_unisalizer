<?php

namespace controllers;

use core\Template;

class Converter extends Base
{
    public $config = [];
    public $params = [];
    public $starting_config = [];
    private $image;


    public function __construct()
    {
        parent::__construct();
        $this->image = USER_DATA_DIR . "images/" . $_SESSION['user_id'] . "/" . date('j-m-y') . "/" . $_SESSION['image'];
    }

    public function action_index()
    {
        if (isset($_POST['file_confirm'])){
            move_uploaded_file($_FILES['image']['tmp_name'], $_SESSION['path'] . $_FILES['image']['name']);
            $_SESSION['image'] = $_FILES['image']['name'];
            $this->image = USER_DATA_DIR . "images/" . $_SESSION['user_id'] . "/" . date('j-m-y') . "/" . $_SESSION['image'];
        }
        list($image_width, $image_height) = getimagesize($this->image);
        $this->layout = 'layouts/base';
        $this->subTemplate = Template::generate('converter',
            [
                'image' => $this->image,
                'image_width' => $image_width,
                'image_height' => $image_height,
            ]
        );
    }

    public function render()
    {
        $this->pageTitle = "Опции";
        parent::render();
    }
}