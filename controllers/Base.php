<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 17.03.2016
 * Time: 21:44
 */
namespace controllers;

use \config\Config;

abstract class Base
{
    protected $layout;
    protected $pageTitle;
    protected $head;
    protected $header;
    protected $menu;
    protected $content;
    protected $footer;
    protected $modals;
    protected $subTemplate;
    protected $js;

    public function __construct()
    {
        $this->js = [
            "jquery-2.2.2.js",
            "main.js",
            "imageprocess.js"
        ];
        $this->header = \core\Template::generate('blocks/header',
            [

            ]);
        $this->footer = \core\Template::generate('blocks/footer');
        $this->modals = \core\Template::generate('blocks/modals');
    }

    public function render()
    {
        $this->head = \core\Template::generate('blocks/head',
            [
                'pageTitle' => Config::$get['site.name'] . " :: " . $this->pageTitle,
                'js' => $this->js,
            ]);
        echo \core\Template::generate($this->layout,
            [
                'pageTitle' => $this->pageTitle,
                'head' => $this->head,
                'header' => $this->header,
                'subTemplate' => $this->subTemplate,
                'footer' => $this->footer,
                'modals' => $this->modals,
            ]);
    }
}