$(function () {
    var result_image_wrapper = $('.result_image_wrapper');
    var doc_w = $(document).width();
    var doc_h = $(document).height();
    var doc_ratio = doc_w / doc_h;
    var img_w;
    var img_h;
    var img_ratio;

    //Обновляем данные о размере окна при ресайзе
    $(window).resize(function () {
        doc_w = $(document).width();
        doc_h = $(document).height();
    });

    $('#param_watermark_button').change(function () {
        $('#param_watermark_button').removeClass("red");
    });

    //Запуск обработки картинки
    $('#run').click(function () {
        $('.preloader').removeClass('hidden');
        $('.button').prop('disabled', true);
        validateWatermark();
        ajaxExecute();
    });

    //Закрытие модального окна с результатом
    $('#closeResult').click(function () {
        closeResult();
    });

    function ajaxExecute(){
        $.ajax({
            url: "../ajax/processor.php",
            type: "POST",
            data: $('.tools').serialize(),
            success: executeSuccess
        });
    }

    function executeSuccess(response) {
        response = JSON.parse(response);
        $('#resultModal').css({
            "opacity" : "1",
            "visibility" : "visible",
            "pointer-events" : "auto"
        });

        $('.preloader').addClass('hidden');
        $('.button').prop('disabled', false);

        result_image_wrapper.find('img').attr('src', response + "?" + (Math.floor(Math.random() * (999 - 100 + 1)) + 100));

        result_image_wrapper.find('img').load(function () {
            var new_w;
            var new_h;
            img_w = this.width;
            img_h = this.height;
            img_ratio = img_w / img_h;

            if (img_ratio > doc_ratio){
                if (img_w > .8 * doc_w){
                    new_w = result_image_wrapper.find('img').width(.8 * doc_w);
                    var k = img_w / new_w;
                    new_h = result_image_wrapper.find('img').height(img_h / k);
                }
            } else {
                if (img_ratio < doc_ratio){
                    if (img_h > .8 * doc_h){
                        new_h = result_image_wrapper.find('img').width(.8 * doc_h);
                        var k = img_h / new_h;
                        new_w = result_image_wrapper.find('img').height(img_w / k);
                    }
                }
            }
        });
    }

    function validateWatermark(){
        if ($('#watermark').prop("checked") == true && $('#param_watermark_base64').val() == ''){
            $('.preloader').addClass('hidden');
            $('.button').prop('disabled', false);
            $('#param_watermark_button').addClass("red");
        }
    }


    function closeResult() {
        result_image_wrapper.find('img').attr('src', '');
        $('#resultModal').css({
            "opacity" : "0",
            "visibility" : "hidden",
            "pointer-events" : "none"
        });
    }
});
