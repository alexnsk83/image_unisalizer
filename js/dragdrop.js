$(function () {
    $.event.props.push('dataTransfer');

    var dropZone = $('#dropZone');

    if (typeof (window.FileReader) == 'undefined') {
        dropZone.text('Не поддерживается браузером!');
        dropZone.addClass('error');
    }

    dropZone[0].ondragover = function () {
        dropZone.addClass('hover');
        return false;
    };

    dropZone[0].ondragleave = function () {
        dropZone.removeClass('hover');
        return false;
    };

    dropZone[0].ondrop = function (event) {
        event.preventDefault();
        dropZone.removeClass('hover');
        dropZone.addClass('drop');
        load_image(event.dataTransfer.files);
    };

    $('#drop_input').on('change', function () {
        dropZone.addClass('drop');
        load_image(this.files);
    });

    function load_image(files){
        var file = files[0];
        var fileReader = new FileReader();

        fileReader.onload = (function (file) {
            return function (e) {
                $('#file_name').val( file.name );
                $('#file_base64').val( this.result );
                console.log($('#file_name').val());
                console.log($('#file_base64').val());
            };
        })(file);

        fileReader.readAsDataURL(file);
    }
});