$(function(){
    var option_list;

    $('input[name="param_watermark_horizontal"]').change(function () {
        if ($('#horcenter').prop('checked')){
            $('#horamount').fadeOut();
        } else {
            $('#horamount').fadeIn();
        }
    });

    $('input[name="param_watermark_vertical"]').change(function () {
        if ($('#vertcenter').prop('checked')){
            $('#vertamount').fadeOut();
        } else {
            $('#vertamount').fadeIn();
        }
    });

    if ($('#horcenter').prop('checked')){
        $('#horamount').css("display","none");
    }

    if ($('#vertcenter').prop('checked')){
        $('#vertamount').css("display","none");
    }

    if ($('#watermark').prop("checked")){
        $('#wm').css("display","block");
    }
    if ($('#lightmult').prop("checked")){
        $('#light').css("display","block");
    }

    $('#image').change(function () {
        $('#file_confirm').removeClass("hidden");
    });

    //Сохранение опций при нажатии на "Сохранить"
    $('.save_button').click(function () {
        $('.action_result_message').html('');           //Очищаем поле для сообщений о статусе загрузки/сохранения
        $('.action_result_message').css('opacity', 1);  //Делаем его видимым
        $('#param_watermark_button').prop('disabled', true);
        options = ($('input[name="options_name"]').serialize() + "&" + $('.tools').serialize());
        ajaxSaveOptions();
        $('#param_watermark_button').prop('disabled', false);
        $('.saveForm').find('.formInput').val('');
    });

    $('#param_watermark_button').change(function(){
        var file = ($(this)[0].files[0]);
        var fileReader = new FileReader();
        var $file_64 = $('#param_watermark_base64');

        fileReader.onload = (function (file) {
            return function (e) {
                $file_64.val(this.result);
                var img = $('#param_watermark_base64').val();
                $('#wm_preview').attr('src', img);
            };
        })(file);

        if (file) {
            fileReader.readAsDataURL(file);
        } else {
            $file_64.val('');
        }
    });

    //При открытии окна загрузки подгружаем доступные опции
    $('a[name="load"]').click(function () {
        ajaxGetOptionsList();
    });

    /**
     * Заргузка установок по нажатию "Загрузить"
     */
    $('#load_button').click(function () {
        $('.options input').prop('checked', false);
        var option_selected = $('.loadForm').find('select option:selected').index();
        var options = (option_list[option_selected].form);
        for (var key in options){
            console.log(options);
            //Т.к. картинки могут быть разного размера, то значения ширины и высоты не загружаются
            if (key == "param_resize_width" || key == "param_resize_height"){
                continue;
            }

            $('#' + key).val(options[key]);         //Расставляем числовые и буквенные значения параметров
            if (options[key] == "on"){              //Расставляем галочки в чекбоксы
                $('#'+key).prop('checked', true);
            }

            if (options[key]){                      //Расставляем галочки в чекбоксы
                $('#'+options[key]).prop('checked', true);
            }

            if (options["param_watermark_base64"]){   //если есть водяной знак - показываем его в превью
                $('#wm_preview').prop('src', options["param_watermark_base64"]);
            }

            if ($('#horcenter').prop('checked')){
                $('#horamount').css("display","none");
            } else {
                $('#horamount').css("display","block");
            }

            if ($('#vertcenter').prop('checked')){
                $('#vertamount').css("display","none");
            } else {
                $('#vertamount').css("display","block");
            }

            if ($('#watermark').prop("checked")){
                $('#wm').css("display","block");
            }
            if ($('#lightmult').prop("checked")){
                $('#light').css("display","block");
            }
        }
        $('.loadForm').find('select').empty();
        location.hash = "#closeModal";
    });

    /**
     * Очищаем сообщения при закрытии модального окна
     */
    $('.closeModal').click(function () {
        $('.action_result_message').html("");
        $('.save_result_message').html("");
        $('.loadForm').find('select').empty();
    });

    //Скрываем настройки водяного знака если эта опция не отмечена
    $('#watermark').change(function () {
        if ($('#watermark').prop('checked') === false){
            $('#wm').slideUp()
        } else {
            $('#wm').slideDown()
        }
    });

    //Скрываем настройки освещения если эта опция не отмечена
    $('#lightmult').change(function () {
        if ($('#lightmult').prop('checked') === false){
            $('#light').slideUp()
        } else {
            $('#light').slideDown()
        }
    });

    $('#param_rotate_angle').change(function () {
        if ($('#param_rotate_angle').val() != 0){
            $('#rotate').prop('checked', true)
        } else {
            $('#rotate').prop('checked', false)
        }
    });

    $('#param_rescale_scale').change(function () {
        if ($('#param_rescale_scale').val() != 100){
            $('#rescale').prop('checked', true)
        } else {
            $('#rescale').prop('checked', false)
        }
    });

    $('#param_reflect_axis').change(function () {
        $('#reflect').prop('checked', true)
    });

    $('#param_resize_width').change(function () {
        $('#resize').prop('checked', true)
    });

    $('#param_resize_height').change(function () {
        $('#resize').prop('checked', true)
    });

    //Очистка сообщения о загрузке/сохранении по таймеру
    $('.action_result_message').change(function () {
        var msg_time = 1;
        if ($('.action_result_message').html() != ''){
            var timer = setInterval(function () {
                msg_time--;
                $('.action_result_message').css('opacity', 0);
                if (msg_time <= 0){
                    clearInterval(timer);
                }
            }, 3000);
        }
    });

    /**
     * Сохраняем выбранные опции
     */
    function ajaxSaveOptions(){
        $.ajax({
            url: "../ajax/save.php",
            type: "POST",
            data: options,
            success: saveSuccess
        });
    }

    /**
     * Достаём сохранённые опции из БД
     */
    function ajaxGetOptionsList(){
        $.ajax({
            url: "../ajax/options_list.php",
            type: "POST",
            success: loadSuccess
        });
    }

    /**Сохранение выбранный опций
     *
     * @param response
     */
    function saveSuccess(response) {
        response = JSON.parse(response);
        if (response.status == "bad"){                              //При ошибке сохранения выводим сообщение в форму
            $('.save_result_message').html(response.result);
        } else {
            $('.saveFormForm').find('input[type="text"]').empty();  //При удачном сохранении закрываем форму
            $('.save_result_message').empty();
            location.hash = "#closeModal";
            $('.action_result_message').html(response.result).change();
        }
    }

    /**Составление списка из сохранённых опций
     *
     * @param result
     */
    function loadSuccess(result) {
        result = JSON.parse(result);
        option_list = result;
        for (var i = 0; i < result.length; i++){
            $('.loadForm').find('select').append("<option>" + result[i]['options_name'] + "</option>")
        }
    }
});