<?php

namespace models;

class Converter extends Base
{
    public static $instance;
    public $im;
    public $format;
    private $im_width;
    private $im_height;
    private $path;

    public static function instance($path)
    {
        if (self::$instance == NULL){
            self::$instance = new self($path);
        }

        return self::$instance;
    }

    public function __construct($path)
    {
        parent::__construct('Converter');

        if (!file_exists($path)){
            echo "Файл не существует";
            echo $path;
            die();
        }

        $this->path = $path;
        $this->im = imagecreatefromstring(file_get_contents($path));
        $this->normalize_image();
        list($this->im_width, $this->im_height) = getimagesize($path);
        $this->detectFormat($path);
    }

    /**
     * @param $name
     * @param $path
     * @param int $id_user
     */
    public function saveImage($name, $path, $id_user = 1)
    {
        $this->db->insert('images',
            [
                'id_user' => $id_user,
                'name' => $name,
                'path' => $path,
            ]);
    }

    /** Наложение эффекта сепии
     * @param int $r
     * @param int $g
     * @param int $b
     */
    public function sepia($r = 50, $g = 30, $b = 0)
    {
        for($i = 0; $i < $this->im_width; $i++){
            for($j = 0; $j < $this->im_height; $j++){
                $color_index = imagecolorat($this->im, $i, $j);
                $change = imagecolorsforindex($this->im, $color_index);
                $this->sepiaAt($change, $r, $g, $b);
                $this->normalize($change);
                $color = imagecolorallocatealpha($this->im, $change['red'], $change['green'], $change['blue'], $change['alpha']);
                imagesetpixel($this->im, $i, $j, $color);
            }
        }
    }

    /**
     * Обесцвечивание изображения
     */
    public function grey()
    {
        for($i = 0; $i < $this->im_width; $i++){
            for($j = 0; $j < $this->im_height; $j++){
                $color_index = imagecolorat($this->im, $i, $j);
                $change = imagecolorsforindex($this->im, $color_index);
                $this->greyAt($change);
                $this->normalize($change);
                $color = imagecolorallocatealpha($this->im, $change['red'], $change['green'], $change['blue'], $change['alpha']);
                imagesetpixel($this->im, $i, $j, $color);
            }
        }
    }

    /**Осветление/затемнение изображения
     * @param $left
     * @param $top
     * @param $right
     * @param $bottom
     */
    public function lightmult($top, $left, $right, $bottom)
    {
        $arr['left'] = $left/100;
        $arr['top'] = $top/100;
        $arr['right'] = $right/100;
        $arr['bottom'] = $bottom/100;
        foreach ($arr as $key => $item){
            if ($item === 0){
                unset($arr[$key]);
            }
        }
        if ($arr){
            foreach ($arr as $key => $value){
                for($i = 1; $i < $this->im_width; $i++){
                    for($j = 1; $j < $this->im_height; $j++){
                        $color_index = imagecolorat($this->im, $i, $j);
                        $change = imagecolorsforindex($this->im, $color_index);
                        switch ($key){
                            case 'left':
                                $this->lightmult_pixel($change, $this->calculateLight(sqrt(($i / $this->im_width)), $value, $value)); //left
                                break;
                            case 'top':
                                $this->lightmult_pixel($change, $this->calculateLight(sqrt(($j / $this->im_height)), $value, $value)); //top
                                break;
                            case 'right':
                                $this->lightmult_pixel($change, $this->calculateLight(-sqrt(($i / $this->im_width)), 0, $value)); //right
                                break;
                            case 'bottom':
                                $this->lightmult_pixel($change, $this->calculateLight(-sqrt(($j / $this->im_height)), 0, $value)); //bottom
                                break;
                        }
                        //$this->lightmult_pixel($change, $this->calculateLight($i / $this->im_width, $j / $this->im_height, $arr));
                        $this->normalize($change);
                        $color = imagecolorallocatealpha($this->im, $change['red'], $change['green'], $change['blue'], $change['alpha']);
                        imagesetpixel($this->im, $i, $j, $color);
                    }
                }
            }

        }

    }

    /** Поворот изображения на выбраный угол с обрезкой краёв
     * @param $angle
     */
    public function rotate($angle)
    {
        $this->im = imagerotate($this->im, $angle, 0);

        $y1 = abs($this->im_width * sin(deg2rad($angle)));
        $y0 = imagesy($this->im) - $y1;

        $x0 = sqrt(pow($this->im_height, 2) - pow($y0, 2));
        $x1 = $this->im_width - $x0;

        if ($y1 > ($this->im_height)/2) {
            list($y0, $y1) = array($y1, $y0);
        }

        if ($x0 > (imagesx($this->im))/2) {
            list($x0, $x1) = array($x1, $x0);
        }

        $this->crop($x0, $y1, $x1-$x0, $y0-$y1);
    }

    /** Обрезка изображения
     * @param $x
     * @param $y
     * @param $width
     * @param $height
     */
    public function crop($x, $y, $width, $height)
    {
        $img = imagecreatetruecolor($width, $height);
        $this->normalize_image($img);
        imagecopy($img, $this->im, 0, 0, $x, $y, $width, $height);
        $this->im = $img;
        $this->im_width = $width;
        $this->im_height = $height;
    }

    /** Зеркальное отражение изображения по выбраным осям
     * ©тырено тут http://forum.php.su/topic.php?forum=71&topic=6700
     * @param string $direction
     */
    public function reflect($direction = 'y')
    {
        $reflected = imagecreatetruecolor($this->im_width, $this->im_height);
        imagealphablending($reflected, false);
        imagesavealpha($reflected, true);

        for ($y = 1; $y <= $this->im_height; $y++)
        {
            for ($x = 0; $x < $this->im_width; $x++)
            {
                if ($direction == 'x')
                {
                    $rgba = imagecolorat($this->im, $this->im_width - ($x + 1), $y - 1);
                } elseif ($direction == 'xy' || $direction == 'yx')
                {
                    $rgba = imagecolorat($this->im, $this->im_width - ($x + 1), $this->im_height - $y);
                } else
                {
                    $rgba = imagecolorat($this->im, $x, $this->im_height - $y);
                }
                $rgba = imagecolorsforindex($this->im, $rgba);
                $rgba = imagecolorallocatealpha($reflected, $rgba['red'], $rgba['green'], $rgba['blue'], $rgba['alpha']);
                imagesetpixel($reflected, $x, $y - 1, $rgba);
            }
        }

        $this->im = $reflected;
    }

    /** Пропорциональное изменение размера изображения
     * @param int $percent
     */
    public function rescale($percent = 100)
    {
        $factor = $percent / 100;
        $new_width = $this->im_width * $factor;
        $new_height = $this->im_height * $factor;
        $new_img = imagecreatetruecolor($new_width, $new_height);
        $this->normalize_image($new_img);
        imagecopyresampled($new_img, $this->im, 0, 0, 0, 0,$new_width, $new_height, $this->im_width, $this->im_height);
        $this->im = $new_img;
        $this->im_width = $new_width;
        $this->im_height = $new_height;
    }

    /** Изменение размера изображения без сохранения пропорций
     * @param int $new_width
     * @param int $new_height
     */
    public function resize($new_width = 100, $new_height = 100)
    {
        $new_img = imagecreatetruecolor($new_width, $new_height);
        $this->normalize_image($new_img);
        imagecopyresampled($new_img, $this->im, 0, 0, 0, 0,$new_width, $new_height, $this->im_width, $this->im_height);
        $this->im = $new_img;
        $this->im_width = $new_width;
        $this->im_height = $new_height;
    }

    /**
     * @param int $right - отступ от правого края
     * @param int $bottom - отступ от нижнего края
     */
    public function watermark($horizontal, $horamount = null, $horunits = null, $vertical, $vertamount = null, $vertunits = null, $base64)
    {
        $image = USER_DATA_DIR . "images/" . $_SESSION['user_id'] . "/" . date('j-m-y') . "/" . "wm" . mt_rand(1000, 9000);
        $data = explode(',', $base64);
        $encodedData = str_replace(' ', '+', $data[1]);
        $decodedData = base64_decode($encodedData);
        
        file_put_contents($image, $decodedData);
        
        if (!file_exists($image)){
            die('Ошибка загрузки водяного знака');
        }
        if (preg_match("/png/", $base64)){
            $this->addWatermarkWithAlpha($image, $horizontal, $horamount, $horunits, $vertical, $vertamount, $vertunits);
        } else {
            $this->addWatermarkWithoutAlpha($image, $horizontal, $horamount, $horunits , $vertical, $vertamount, $vertunits);
        }

    }

    /**
     * @return string
     */
    public function render($path = null)
    {
        ob_start();
        switch ($this->format){
            case 'png':
                imagepng($this->im, $path);
                break;
            case 'gif':
                imagegif($this->im, $path);
                break;
            default:
                imagejpeg($this->im, $path);
                break;
        }

        return ob_get_clean();
    }

    /**
     * @param $path - Возвращает расширение файла
     */
    private function detectFormat($path)
    {
        $data = explode('.', $path);
        $this->format = array_pop($data);
    }


    private function addWatermarkWithAlpha($mark, $horizontal, $horamount = null, $horunits = null, $vertical, $vertamount = null, $vertunits = null)
    {
        $wm_img = imagecreatefrompng($mark);
        list($wm_width, $wm_height) = getimagesize($mark);
        $wmx_offset = $wmy_offset = 0;

        switch ($horizontal){
            case "horright":
                $wmx = $this->im_width - $wm_width;
                $horamount = -$horamount;
                break;
            case "horcenter":
                $wmx = $this->im_width / 2 - $wm_width / 2;
                $horamount = 0;
                break;
            default:
                $wmx = 0;
                break;
        }

        if (isset($horunits)){
            switch ($horunits){
                case "px":
                    $wmx_offset = $horamount;
                    break;
                default:
                    $wmx_offset = $this->im_width / 100 * $horamount;
                    break;
            }
        }

        switch ($vertical){
            case "vertbottom":
                $wmy = $this->im_height - $wm_height;
                $vertamount = -$vertamount;
                break;
            case "vertcenter":
                $wmy = $this->im_height / 2 - $wm_height / 2;
                $vertamount = 0;
                break;
            default:
                $wmy = 0;
                break;
        }

        if (isset($vertunits)){
            switch ($vertunits){
                case "px":
                    $wmy_offset = $vertamount;
                    break;
                default:
                    $wmy_offset = $this->im_height / 100 * $vertamount;
                    break;
            }
        }

        imagealphablending($this->im, true);

        imagecopy($this->im, $wm_img,
            $wmx + $wmx_offset, $wmy + $wmy_offset,
            0, 0,
            $wm_width, $wm_height);

        imagealphablending($this->im, false);
    }


    private function addWatermarkWithoutAlpha($mark, $horizontal, $horamount = null, $horunits = null, $vertical, $vertamount = null, $vertunits = null)
    {
        $wm_img = imagecreatefromstring(file_get_contents($mark));
        list($wm_width, $wm_height) = getimagesize($mark);
        $wmx_offset = $wmy_offset = 0;

        switch ($horizontal){
            case "horright":
                $wmx = $this->im_width - $wm_width;
                $horamount = -$horamount;
                break;
            case "horcenter":
                $wmx = $this->im_width / 2 - $wm_width / 2;
                $horamount = 0;
                break;
            default:
                $wmx = 0;
                break;
        }

        if (isset($horunits)){
            switch ($horunits){
                case "px":
                    $wmx_offset = $horamount;
                    break;
                default:
                    $wmx_offset = $this->im_width / 100 * $horamount;
                    break;
            }
        }

        switch ($vertical){
            case "vertbottom":
                $wmy = $this->im_height - $wm_height;
                $vertamount = -$vertamount;
                break;
            case "vertcenter":
                $wmy = $this->im_height / 2 - $wm_height / 2;
                $vertamount = 0;
                break;
            default:
                $wmy = 0;
                break;
        }

        if (isset($vertunits)){
            switch ($vertunits){
                case "px":
                    $wmy_offset = $vertamount;
                    break;
                default:
                    $wmy_offset = $this->im_height / 100 * $vertamount;
                    break;
            }
        }

        imagecopy($this->im, $wm_img,
            $wmx + $wmx_offset, $wmy + $wmy_offset,
            0, 0,
            $wm_width, $wm_height);
    }

    /**
     * @param $color
     * @param $r
     * @param $g
     * @param $b
     */
    private function sepiaAt(&$color, $r, $g, $b)
    {
        $avg = ($color['red'] + $color['green'] + $color['blue']) / 3;
        $color['red'] = $avg + $r;
        $color['green'] = $avg + $g;
        $color['blue'] = $avg + $b;
    }

    /**
     * @param $color
     */
    private function greyAt(&$color)
    {
        $avg = ($color['red'] + $color['green'] + $color['blue']) / 3;
        $color['red'] = $color['green'] = $color['blue'] = $avg;
    }

    /**
     * @param $color
     * @param $n
     */
    private function lightmult_pixel(&$color, $n)
    {
        $color['red'] *= $n;
        $color['red'] = $color['red'] < 0 ? 0 : $color['red'];
        $color['red'] = $color['red'] > 255 ? 255 : $color['red'];
        $color['green'] *= $n;
        $color['green'] = $color['green'] < 0 ? 0 : $color['green'];
        $color['green'] = $color['green'] > 255 ? 255 : $color['green'];
        $color['blue'] *= $n;
        $color['blue'] = $color['blue'] < 0 ? 0 : $color['blue'];
        $color['blue'] = $color['blue'] > 255 ? 255 : $color['blue'];
    }

    private function calculateLight($xy, $val1, $val2){
        return (1 + $val1) - ($xy * $val2);
    }

    /**
     * @param $color
     */
    private function normalize(&$color)
    {
        foreach ($color as $channel => $value) {
            $color[$channel] = ($value > 255) ? 255 : $color[$channel];
            $color[$channel] = ($value < 0) ? 0 : $color[$channel];
        }
    }

    /**
     * @param null $img
     */
    private function normalize_image($img = null){
        $img = $img ? $img : $this->im;
        imagealphablending($img, false);
        imagesavealpha($img, true);
    }
}