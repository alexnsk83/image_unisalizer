<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.03.2016
 * Time: 18:40
 */

namespace models;

use \core\SQL;

abstract class Base
{
    protected $db;
    protected $table;

    public function __construct($table)
    {
        $this->db = SQL::instance();
        $this->table = $table;
    }

    public function getAll()
    {
        return $this->db->select("SELECT * FROM {$this->table}");
    }

    public function getById($id)
    {
        return $this->db->selectOne("SELECT * FROM {$this->table} WHERE id = $id");
    }

    public function delete($id)
    {
        $this->db->delete("DELETE FROM {$this->table} WHERE id = $id");
    }

    public function checkSame($array)
    {
        $string = null;
        foreach ($array as $key => $value){
            $string .= "$key = '$value' AND ";
        }
        $string = substr($string, 0, -4);
        $result = $this->db->select("SELECT * FROM {$this->table} WHERE $string ");
        if ($result){
            return true;
        }
        return false;
    }
}