<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.09.2016
 * Time: 11:31
 */

namespace models;


class Options extends Base
{
    public static $instance;

    public static function instance()
    {
        if (self::$instance == NULL){
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        parent::__construct('options');
    }

    public function saveOptions($id_user, $name, $options)
    {
        return $this->db->insert('options', [
            'id_user' => $id_user,
            'name' => $name,
            'options' => $options,
        ]);
    }

    public function getByCondition($condition_name, $condition_value)
    {
        return $this->db->select("SELECT * FROM {$this->table} WHERE {$condition_name} = {$condition_value} ORDER BY name");
    }
}