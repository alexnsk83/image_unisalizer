<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.03.2016
 * Time: 18:40
 */

namespace models;

class Users extends Base
{
    private static $instance;

    public static function instance()
    {
        if (self::$instance == NULL){
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct()
    {
        parent::__construct('Users');
    }

    //Вход по логину и паролю
    public function getSpecified($login, $password)
    {
        return $this->db->selectOne("SELECT * FROM users WHERE login = '$login' AND password = '$password'");
    }

    //Регистрация нового пользователя
    public function add($login, $email, $password)
    {
        $lastUserID = $this->db->insert('users', [
            'password' => $password,
            'email' => $email,
            'login' => $login,
        ]);
        $code = $_SERVER['HTTP_ORIGIN'] . "/users/activate/?code=" . md5($password . $email . time()) . "&id=" . $lastUserID;
        $this->db->insert('registers',[
            'code' => $code,
            'id_user' => $lastUserID,
        ]);

        $this->sendMail($email, $code);
    }

    public function activate($code, $id)
    {
        if ($this->db->selectOne("SELECT id_user FROM registers WHERE code = '$code' AND id_user = $id")){
            $this->db->update('users', ['is_active' => 1], "id_user = $id" );
            $this->db->delete("DELETE FROM registers WHERE id_user = $id");

            return true;
        }
    }

    //Проверка совпадения логинов
    public function checkSameLogin($login)
    {
        $q = $this->db->selectOne("SELECT login FROM users WHERE login = '$login'");
        if ($q) {

            return true;
        }

        return false;
    }

    //Проверка соврадения электронки
    public function checkSameEmail($email)
    {
        $q = $this->db->selectOne("SELECT email FROM users WHERE email = '$email'");
        if ($q) {

            return true;
        }

        return false;
    }

    //Выход из учётки
    public function logout()
    {
        setcookie('remember', '', null, "/");
        setcookie('login', '', null, "/");
        setcookie('key', '', null, "/");
        $_SESSION = [];
        session_regenerate_id();
        $_SESSION['login'] = 'Гость';
        redirectTo('/');
    }

    public function truncateUsers()
    {
        $this->db->truncate("TRUNCATE `users`");
        $this->db->truncate("TRUNCATE `registers`");
    }

    private function sendMail($address, $string)
    {
        $mail = new \PHPMailer();
        $mail->Host = \config\Config::$mail['host'];
        $mail->SMTPAuth = true;
        $mail->Username = \config\Config::$mail['username'];
        $mail->Password = \config\Config::$mail['pass'];
        $mail->SMTPSecure = 'ssl';
        $mail->Port = \config\Config::$mail['port'];
        $mail->CharSet = \config\Config::$mail['charset'];
        $mail->From = \config\Config::$mail['from'];
        $mail->FromName = \config\Config::$mail['from_name'];
        $mail->Subject = 'Активация учётной записи';
        $mail->msgHTML('<a href="'.$string.'">'.$string.'</a>');
        $mail->addAddress($address);
        $mail->send();
    }
}