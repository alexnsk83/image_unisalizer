<?php
/**
 *Основной конфиг
 * Написано и местами ©пёрто Алексеем Балобановым
 * 2015г
 */

namespace config;

class Config
{
    public static $get = [
        'mysql.host' => 'localhost',
        'mysql.login' => 'root',
        'mysql.pass' => '',
        'mysql.name' => 'uniq_image',
        'site.name' => 'Уникализатор',
    ];

    public static $mail = [
        'host' => 'localhost',
        'username' => 'root',
        'pass' => '',
        'port' => 465,
        'charset' => 'UTF-8',
        'from' => 'example@example.com',
        'from_name' => 'Уникализатор',

    ];
}
