<div class="converter_wrapper">
    <div class="preloader hidden"></div>
    <main class="main_content">
        <img class="source_image" src="/userdata/images/<?= $_SESSION['user_id'] . "/" . date('j-m-y') . "/" . $_SESSION['image'] ?>">
        <div class="tools_panel">
            <form class="tools" method="post" enctype="multipart/form-data">
                <div class="options">
                    <input type="checkbox" id="grey" name="grey" <?php if(($_POST['grey'] == "on")) echo "checked" ?>><label for="grey">Обесцветить</label><br>
                    <input type="checkbox" id="sepia" name="sepia" <?php if(($_POST['sepia'] == "on")) echo "checked" ?>><label for="sepia">Сепия</label><br>
                    <input type="checkbox" id="rotate" name="rotate" <?php if(($_POST['rotate'] == "on")) echo "checked" ?>><label for="rotate">Поворот</label><input type="text" id="param_rotate_angle" name="param_rotate_angle" value="0"><sup>o</sup><br>
                    <input type="checkbox" id="reflect" name="reflect" <?php if(($_POST['reflect'] == "on")) echo "checked" ?>><label for="reflect">Отразить по оси</label><input id="param_reflect_axis" type="text" name="param_reflect_axis" value="x"><br>
                    <input type="checkbox" id="rescale" name="rescale" <?php if(($_POST['rescale'] == "on")) echo "checked" ?>><label for="rescale">Изменить масштаб</label><input type="text" id="param_rescale_scale" name="param_rescale_scale" value="100"><span>%</span><br>

                    <input type="checkbox" id="resize" name="resize" <?php if(($_POST['resize'] == "on")) echo "checked" ?>><label for="resize">Изменить размеры</label>
                    <input type="text" id="param_resize_width" name="param_resize_width" datatype="size" placeholder="ширина" value="<?= $image_width ?>"><sub>px</sub>
                    <input type="text" id="param_resize_height" name="param_resize_height" datatype="size" placeholder="высота" value="<?= $image_height ?>"><sub>px</sub><br>

                    <input type="checkbox" id="lightmult" name="lightmult" <?php if(($_POST['lightmult'] == "on")) echo "checked" ?>><label for="lightmult">Изменить яркость, %</label><br>
                    <div id="light">
                        <label for="param_lightmult_t" class="top_bottom">Сверху</label><input type="text" id="param_lightmult_t" name="param_lightmult_t" value="0"><br>
                        <label for="param_lightmult_l" class="sub_option">Слева</label><input type="text" id="param_lightmult_l" name="param_lightmult_l" value="0">
                        <input type="text" id="param_lightmult_r" name="param_lightmult_r" value="0"><label for="param_lightmult_r" class="sub_option right">Справа</label><br>
                        <label for="param_lightmult_b" class="top_bottom"> Снизу </label><input type="text" id="param_lightmult_b" name="param_lightmult_b" value="0"><br>
                    </div>

                    <input type="checkbox" id="watermark" name="watermark" <?php if(($_POST['watermark'] == "on")) echo "checked" ?>><label for="watermark">Добавить водяной знак</label><br>
                    <div id="wm" class="clearfix">
                        <span class="radio_as_buttons clearfix">
                            <div>
                                <input type="radio" name="param_watermark_horizontal" id="horcenter" value="horcenter" class="hidden" checked>
                                <label for="horcenter">по центру</label>
                            </div>
                            <div>
                                <input type="radio" name="param_watermark_horizontal" id="horleft" value="horleft" class="hidden">
                                <label for="horleft">слева</label>
                            </div>
                            <div>
                                <input type="radio" name="param_watermark_horizontal" id="horright" value="horright" class="hidden">
                                <label for="horright">справа</label>
                            </div>
                            <div id="horamount">
                                <input type="text" name="param_watermark_horamount" id="param_watermark_horamount" value="0">
                                <select id="param_watermark_horunits" name="param_watermark_horunits">
                                    <option>px</option>
                                    <option>%</option>
                                </select>
                            </div>
                        </span>
                        <span class="radio_as_buttons clearfix">
                            <div>
                                <input type="radio" name="param_watermark_vertical" value="vertcenter" id="vertcenter" class="hidden" checked>
                                <label for="vertcenter">по центру</label>
                            </div>
                            <div>
                                <input type="radio" name="param_watermark_vertical" value="verttop" id="verttop" class="hidden">
                                <label for="verttop">сверху</label>
                            </div>
                            <div>
                                <input type="radio" name="param_watermark_vertical" value="vertbottom" id="vertbottom" class="hidden">
                                <label for="vertbottom"> снизу </label>
                            </div>
                            <div id="vertamount">
                                <input type="text" name="param_watermark_vertamount" id="param_watermark_vertamount" value="0">
                                <select id="param_watermark_vertunits" name="param_watermark_vertunits">
                                    <option>px</option>
                                    <option>%</option>
                                </select>
                            </div>
                        </span>
                        <input type="hidden" id="param_watermark_base64" name="param_watermark_base64" value="<?= $_POST['param_watermark_base64'] ?>">
                        <input type="file" id="param_watermark_button" name="param_watermark_button" class="sub_option "><br>
                        <img id="wm_preview" class="wm_preview" src="<?= $_POST['param_watermark_base64'] ?>">
                    </div>
                </div>
                <div class="buttons_panel">
                    <input type="button" id="run" name="run" class="button" value="Применить"><br>
                    <label class="button" id="image_label" for="image">Сменить картинку</label>
                    <input type="file" name="image" class="hidden" id="image" accept="image/*">
                    <?php if (isset($imageErrors['image'])){ ?><div class="errorMsg"><?= $imageErrors['image'];?></div><?}?><br>
                    <input type="submit" class="button confirm hidden" id="file_confirm" name="file_confirm" value="Подтвердить">
                </div>
            </form>
        </div>
    </main>

    <?php
    if ($_SESSION['user_id'] != 0) { ?>
        <div class="save_load_buttons">
            <a href="#loadModal" class="button" name="load">Загрузить установки</a>
            <a href="#saveModal" class="button" name="save">Сохранить установки</a>
            <div class="action_result_message"></div>
        </div>
    <?php } ?>
</div>