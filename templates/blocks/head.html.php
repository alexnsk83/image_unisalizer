    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=0.1">
    <meta name="theme-color" content="#12A3EB">
    <link rel="stylesheet" href="/templates/styles/main.css?<?php echo mt_rand(100, 1000); ?>">
    <?php
        foreach ($js as $file){?>
            <script src="/js/<?php echo $file . "?" . mt_rand(100, 1000); ?>"></script>
        <?php } ?>
    <title><?= $pageTitle ?></title>