<div class="wrapper">
    <header>
        <div class="auth clearfix">
            <?php
            if (!isset($_SESSION['auth'])){?>
                <a class="button menu_button clearfix" href="/users/registration/">Регистрация</a>
                <a class="button menu_button clearfix" href="/users/login">Вход</a>
            <?php } else { ?>
                <a class="button menu_button clearfix" href="/users/logout/">Выход</a>
            <?php } ?>

            <div class="header_msg">
                <?php
                if (isset($_SESSION['reg_ok_msg'])){
                    echo $_SESSION['reg_ok_msg'];
                    unset($_SESSION['reg_ok_msg']);
                } else {
                    echo "Вы вошли как "; ?><b><?=$_SESSION['login']?></b><?php
                }
                ?>
            </div>
        </div>
    </header>