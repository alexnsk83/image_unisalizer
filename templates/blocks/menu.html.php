<div class="menu">
    <div class="search">
        <form method="get">
            <input type="text" name="search_request" placeholder="Поиск">
            <input type="submit" class="submit"><br>
            <input type="radio" checked name="where" id="in_catalog"><label for="in_catalog">в товарах</label>
            <input type="radio" name="where" id="in_articles"><label for="in_articles">в публикациях</label>
        </form>
    </div>
    <nav>
        <ul class="compact">
            <li><div class="green nbut"><a href="#">Меню</a>
                    <ul class="hidden_menu">
                        <li><div class="green nbut"><a href="#">Мастер-классы</a></div></li>
                        <li><div class="green nbut"><a href="#">Интересное</a></div></li>
                        <li><div class="green nbut"><a href="#">Полезное</a></div></li>
                        <li><div class="green nbut"><a href="#">Контакты</a></div></li>
                        <li><div class="green nbut"><a href="#">Доставка и оплата</a></div></li>
                        <li><div class="green nbut"><a href="#">Новости</a></div></li>
                    </ul>
                </div>
            </li>
            <li><div class="orange feedback nbut"><a href="#">Обратная связь</a></div></li>
        </ul>
        <ul class="fullsize">
            <li><div class="green nbut"><a href="#">Мастер-классы</a></div></li>
            <li><div class="green nbut"><a href="#">Интересное</a></div></li>
            <li><div class="green nbut"><a href="#">Полезное</a></div></li>
            <li><div class="green nbut"><a href="#">Контакты</a></div></li>
            <li><div class="green nbut"><a href="#">Доставка и оплата</a></div></li>
            <li><div class="orange nbut"><a href="#">Обратная связь</a></div></li>
        </ul>
    </nav>
</div>