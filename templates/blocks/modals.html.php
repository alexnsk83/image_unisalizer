<div id="saveModal" class="modal">
    <div class="modal-wrapper">
        <a href="#closeModal" class="closeModal">X</a>
        <form class="saveForm" method="post">
            <input type="text" class="formInput" name="options_name" placeholder="Введите имя профиля">
            <input type="button" class="save_button button" name="options_save" value="Сохранить">
        </form>
        <div class="save_result_message"></div>
    </div>
</div>
<div id="loadModal" class="modal">
    <div class="modal-wrapper">
        <a href="#closeModal" class="closeModal">X</a>
        <form class="loadForm" method="post">
            <select class="formInput"></select>
            <input type="button" id="load_button" class="load_button button" name="options_load" value="Загрузить">
        </form>
    </div>
</div>
<div id="loginModal" class="modal">
    <div class="modal-wrapper login_modal-wrapper">
        <a href="#closeModal" class="closeModal">X</a>
        <form class="loginForm" method="post">
            <input type="text" class="formInput" name="login" placeholder="Введите email"><br>
            <input type="password" class="formInput" name="password" placeholder="Введите пароль"><br>
            <input type="checkbox" id="remember" class="remember" name="remember" value="remember">
            <label for="remember" class="remember">Запомнить меня</label>
            <input type="submit" class="log_in" name="log_in" value="Войти"><br>
            <?php if (isset($loginErrors['wrong'])){ ?><div class="errorMsg"><?php echo $loginErrors['wrong'];?></div><?}?>
        </form>
    </div>
</div>
<div id="resultModal" class="modal">
    <div class="result_image_wrapper">
        <div id="closeResult" class="closeModal">X</div>
        <img>
    </div>

</div>