<aside class="side right">
    <div class="col_tab"><a href="#">Новости</a></div>
    <div class="news_block">
        <div class="news_preview"><a href="#">До окончания конкурса осталось 15 дней! Успейте выложить свою работу!</a></div>
        <div class="news_preview"><a href="#">Друзья и соратники, мы начинаем подготовительный этап работы над проектом парка ремесел! </a></div>
        <div class="news_preview"><a href="#">«РОДНИКИ» - первый в Новосибирске фестиваль родной культуры такого масштаба. Это некоммерческий проект.</a></div>
    </div>
    <div class="new_product">
        <a href="#">
            <div class="new_product_photo"></div>
            <div class="new_product_name">"Одолень-Трава" красная на белом</div>
            <div class="new_product_price">35 руб.</div>
        </a>
    </div>
</aside>
</div>