<div class="content">
    <aside class="side">
        <div class="col_tab"><a href="#">Товары</a></div>
        <div class="catalog">
            <ul>
                <li class="cat-1"><a href="#">Традиционная обрядовая одежда</a>
                    <ul class="subcat">
                        <li><a href="#">Обереговые пояса</a></li>
                        <li><a href="#">Очелья</a></li>
                        <li><a href="#">Обручья</a></li>
                        <li><a href="#">Обереги</a></li>
                    </ul>
                </li>
                <li class="cat-2"><a href="#">Этническая одежда</a>
                    <ul class="subcat">
                        <li><a href="#">Вязаная одежда</a></li>
                        <li><a href="#">Варежки, шапки, шарфы</a></li>
                        <li><a href="#">Украшения</a></li>
                        <li><a href="#">Сумки</a></li>
                    </ul>
                </li>
                <li class="cat-3"><a href="#">Товары для рукоделия</a>
                    <ul class="subcat">
                        <li><a href="#">Обереговые пояса</a></li>
                        <li><a href="#">Лён</a></li>
                        <li><a href="#">Мулине</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </aside>