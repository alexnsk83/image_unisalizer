<main class="main_content">
    <div class="register">
        <h1>Регистрация</h1>
        <form class="register_form" method="post">
            <input type="email" class="formInput" name="email" required placeholder="E-mail" value="<? if(isset($_POST['email'])){echo $_POST['email'];}?>"><span class="red">*</span>
            <?php if (isset($registerErrors['email'])){ ?><div class="errorMsg"><?= $registerErrors['email'];?></div><?}?><br>
            <input type="password" class="formInput" name="password" required placeholder="Пароль"><span class="red">*</span>
            <?php if (isset($registerErrors['password'])){ ?><div class="errorMsg"><?= $registerErrors['password'];?></div><?}?><br>
            <input type="password" class="formInput" name="password_repeat" required placeholder="Повтор пароля"><span class="red">*</span>
            <?php if (isset($registerErrors['password_repeat'])){ ?><div class="errorMsg"><?= $registerErrors['password_repeat'];?></div><?}?><br>
            <input type="submit" class="button" value="Зарегистрироваться">
        </form>
    </div>
</main>