<main class="image_load">
    <h2>Загрузите изображение</h2>
    <form method="post" class="form_on_index" enctype="multipart/form-data" action="/main">
        <label for="drop_input" id="dropZone">Нажми или перетащи сюда файл</label>
        <input type="file" id="drop_input" class="hidden" name="image" accept="image/*">
        <?php if (isset($imageErrors['image'])){ ?><div class="errorMsg"><?= $imageErrors['image'];?></div><?}?><br>
        <input type="text" name="file_name" id="file_name" class="hidden">
        <input type="text" name="file_base64" id="file_base64" class="hidden">
        <input type="submit" class="submit_on_index" value="Вперёд">
    </form>
</main>