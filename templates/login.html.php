<main class="main_content">
    <div class="register">
        <h1>Вход</h1>
        <form class="register_form" method="post">
            <input type="email" class="formInput" name="email" required placeholder="E-mail" value="<? if(isset($_POST['email'])){echo $_POST['email'];}?>"><br>
            <input type="password" class="formInput" name="password" required placeholder="Пароль"></span>
            <?php if (isset($loginErrors['wrong'])){ ?><div class="errorMsg"><?= $loginErrors['wrong'];?></div><?}?><br>
            <input type="checkbox" id="remember" class="remember" name="remember">
            <label for="remember" class="remember">Запомнить меня</label><br>
            <input type="submit" class="button" name="log_in" value="Войти">
        </form>
    </div>
</main>