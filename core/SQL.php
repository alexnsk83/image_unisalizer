<?php

namespace Core;

use \config\Config;

class SQL
{
    private static $instance;
    private $db;

    public static function instance()
    {
        if(self::$instance == NULL){
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        $dsn = 'mysql:host=' . Config::$get['mysql.host'] . ';dbname=' . Config::$get['mysql.name'] . ';charset=utf8';
        $options = [
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_ERRMODE =>\PDO::ERRMODE_EXCEPTION,
        ];

        try {
            setlocale(LC_ALL, 'ru_RU.utf8');
            $this->db = new \PDO($dsn, Config::$get['mysql.login'], Config::$get['mysql.pass'], $options);
        } catch (\PDOException $e) {
            die ('Не удалось подключиться ' . $e->getMessage());
        }
    }

    //Запрос SELECT если результатов может быть больше одного
    public function select($query)
    {
        try {
            $q = $this->db->prepare($query);
            $q->execute();

            return $q->fetchAll();
        } catch (\PDOException $e){
            $msg = 'Запрос SELECT не удался.';
        }
    }

    //Запрос SELECT если результатов выборки может быть 0 или 1.
    public function selectOne($query)
    {
        try {
            $q = $this->db->prepare($query);
            $q->execute();

            return $q->fetch();
        } catch (\PDOException $e) {
            $msg = 'Запрос SELECT ONE не удался.';
        }
    }

    public function insert($table, $obj)
    {
        $columns = [];
        $masks = [];

        foreach ($obj as $key => $value) {
            $columns[] = $key;
            $masks[] = ":$key";

            if ($value == NULL){
                $obj[$key] = 'NULL';
            }
        }

        $columns_res = implode(',', $columns);
        $masks_res = implode(',', $masks);

        $query = "INSERT INTO $table ($columns_res) VALUES ($masks_res)";
        try {
            $q = $this->db->prepare($query);
            $q->execute($obj);

            return $this->db->lastInsertId();
        } catch (\PDOException $e) {
            echo 'Запрос INSERT не удался.' . $e->getMessage();
        }
    }

    public function update($table, Array $obj, $where = '')
    {
        $sets = [];
        foreach ($obj as $key => $value) {
            $sets[] = "$key=:$key";
            if ($value == null) {
                $obj[$key] = 'NULL';
            }
        }

        $sets_s = implode(',', $sets);
        $query = "UPDATE $table SET $sets_s WHERE $where";

        try {
            $q = $this->db->prepare($query);
            $q->execute($obj);

            return $q->rowCount();
        } catch (\PDOException $e) {
            die('Запрос провалился ' . $e->getMessage());
        }
    }

    public function delete($query)
    {
        try {
            $q = $this->db->prepare($query);
            $q->execute();

        } catch (\PDOException $e){
            $msg = 'Запрос DELETE не удался.';
        }
    }

    public function truncate($query){
        try {
            $q = $this->db->prepare($query);
            $q->execute();

        } catch (\PDOException $e){
            $msg = 'Запрос TRUNCATE не удался.';
        }
    }
}