<?php

define ('ROOT_DIR', str_replace('\\', '/', dirname(__DIR__)));
define ('CONFIG_DIR', ROOT_DIR . '/config/');
define ('USER_DATA_DIR', ROOT_DIR . '/userdata/');
define ('CORE_DIR', ROOT_DIR . '/core/');
define ('MODELS_DIR', ROOT_DIR . '/models/');
define ('CONTROLLERS_DIR', ROOT_DIR . '/controllers/');
define ('IMAGES_DIR', ROOT_DIR . '/images/');
define ('TEMPLATES_DIR', ROOT_DIR . '/templates/');
define ('INCLUDES_DIR', ROOT_DIR . '/includes/');
define ('AJAX_DIR', ROOT_DIR . '/ajax/');