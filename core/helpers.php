<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.03.2016
 * Time: 19:35
 */

function dump($var) //Nice-formatted dump output by Dmitry Yuriev
{
    ob_start();
    var_dump($var);
    $dump = ob_get_contents();
    ob_end_clean();

    ob_start();
    highlight_string("<?php\n" . $dump . "\n?>");
    $highlightedDump = ob_get_contents();
    ob_end_clean();

    echo '<pre><strong><div style="padding:10px;background: rgba(204, 204, 204, 0.16);border: 1px dashed black;display: inline-block;">',
    str_replace(['&lt;?php', '?&gt;'], '', trim($highlightedDump)),
    '</div></strong></pre>';
}

//Редирект
function redirectTo($url)
{
    header('Location: ' . $url);
    die();
}