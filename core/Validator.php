<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 24.03.2016
 * Time: 0:50
 */

namespace core;


class Validator
{
    public static function extractArray($arr, $fields){
        $clearArr = [];
        foreach ($fields as $field){
            array_key_exists($field, $arr) ? $clearArr[$field] = $arr[$field] : $clearArr[$field] = null;
        }

        return self::run($clearArr);
    }

    private static function getRule()
    {
        return [
            'caption' => ['not_empty', 'max_length' => 50, 'min_length' => 5],
            'content' => ['not_empty', 'min_length' => 100],
            'login' => ['not_empty', 'login_exists'],
            'email' => ['not_empty', 'email_exists'],
            'password' => ['not_empty', 'min_length' => 3],
            'password_repeat' => ['equal_to_password'],
            'first_name' => ['not_empty', 'max_length' => 50, 'min_length' => 1],
            'last_name' => ['not_empty', 'max_length' => 50, 'min_length' => 1],
            'image' => ['file_selected', 'allowed_format'],
        ];
    }


    private static function run($obj)
    {
        $rules = self::getRule();
        $errors = [];

        foreach ($obj as $field => $rule) {
            if (isset($rules[$field])) {

                // проверяем пустоту
                if (in_array('not_empty', $rules[$field])) {
                    if ($rule === '' || $rule === null) {
                        $errors[$field] = "Это поле не должно быть пустым!";
                        continue;
                    }
                }

                // проверяем выбран ли файл
                if (in_array('file_selected', $rules[$field])) {
                    if ($rule['name'] === '' || $rule === null) {
                        $errors[$field] = "Необходимо выбрать файл";
                        continue;
                    }
                }

                // проверяем максимальную длину
                if (array_key_exists('max_length', $rules[$field])) {
                    if (strlen($rule) > (int)$rules[$field]['max_length']) {
                        $errors[$field] = "Длина поля не должна превышать {$rules[$field]['max_length']}";
                    }
                }

                // проверяем минимальную длину
                if (array_key_exists('min_length', $rules[$field])) {
                    if (strlen($rule) < (int)$rules[$field]['min_length']) {
                        $errors[$field] = "Длина поля не должна быть меньше чем {$rules[$field]['min_length']}";
                    }
                }

                //проверяем пароли на совпадение
                if (in_array('equal_to_password', $rules[$field])){
                    if ($rule != $obj['password']){
                        $errors[$field] = "Пароли не совпадают";
                    }
                }

                //проверяем свободен ли логин
                if (in_array('login_exists', $rules[$field])){
                    $res = \models\Users::instance()->checkSameLogin($rule);
                    if ($res){
                        $errors[$field] = "Это имя пользователя уже занято";
                    }
                }

                //проверяем свободен ли адрес
                if (in_array('email_exists', $rules[$field])){
                    $res = \models\Users::instance()->checkSameEmail($rule);
                    if ($res){
                        $errors[$field] = "Это адрес уже зарегистрирован";
                    }
                }

                //проверка допустимости формата изображения
                if (in_array('allowed_format', $rules[$field])){
                    if (!in_array($rule['type'],['image/jpeg', 'image/png', 'image/gif'])){
                        $errors[$field] = "Неверный формат изображения";
                    }
                }
            }
        }

        if (!empty($errors)) {
            return $errors;
        }

        return false;
    }
}