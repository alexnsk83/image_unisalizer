<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 17.03.2016
 * Time: 21:51
 */
namespace core;

class Template
{
    public static function generate($path, $data=[]){
        foreach ($data as $key => $value){
            $$key = $value;
        }

        ob_start();
        include_once TEMPLATES_DIR . $path . '.html.php';
        return ob_get_clean();
    }
}