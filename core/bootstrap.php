<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 17.03.2016
 * Time: 20:16
 */
session_start();
require_once ('defines.php');
require_once ('helpers.php');
require_once ('SQL.php');
require_once ('Validator.php');
require_once (MODELS_DIR . 'Base.php');
require_once (MODELS_DIR . 'Users.php');
include_once (INCLUDES_DIR . 'PHPMailer/class.phpmailer.php');
include_once (INCLUDES_DIR . 'PHPMailer/class.smtp.php');
require_once (CONFIG_DIR . 'config.php');


//Если пользователь не авторизован, присваиваем ему роль - Гость
if (!isset($_SESSION['auth'])){
    $_SESSION['login'] = 'Гость';
    $_SESSION['user_id'] = 0;
}

//Автоматическая авторизация, если был установлен checkbox "Запомнить меня"
if (!isset($_SESSION['auth']) && isset($_COOKIE['remember'])){
    $users = [];
    $users = \models\Users::instance()->getAll();
    foreach ($users as $user) {
        if ($_COOKIE['key'] == md5(12345 . $user['password'])) {
            $_SESSION['auth'] = true;
            $_SESSION['login'] = $user['login'];
            $_SESSION['user_id'] = $user['id_user'];
            break;
        }
    }
}

//Обработка клика по кнопке "Выйти"
if (isset($_GET['exit']) && $_GET['exit'] == 'exit') {
    \models\Users::instance()->logout();
}