<?php
include_once ('../config/config.php');
include_once ('../core/SQL.php');
include_once('../models/Base.php');
include_once('../models/Options.php');
session_start();

$response = ['status' => 'none', 'result' => 'none'];

$mOptions = new \models\Options();

if ($_POST['options_name']){

    if($mOptions->checkSame(['name' => $_POST['options_name'], 'id_user' => $_SESSION['user_id']])){
        $response['status'] = "bad";
        $response['result'] = "Запись с таким именем уже существует";
    } else {
        $post = $_POST;
        foreach ($post as $key => $value){
            
        }
        if ($mOptions->saveOptions($_SESSION['user_id'], $post['options_name'], json_encode($post))) {
            $response['status'] = "ok";
            $response['result'] = "Настройки сохранены";
        } else {
            $response['status'] = "bad";
            $response['result'] = "Ошибка сохранения";
        }
    };
} else {
    $response['status'] = "bad";
    $response['result'] = "Не указано имя профиля";
}


echo json_encode($response);