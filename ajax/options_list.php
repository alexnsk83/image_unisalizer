<?php
include_once ('../config/config.php');
include_once ('../core/SQL.php');
include_once('../models/Base.php');
include_once('../models/Options.php');
session_start();

$response = ['status' => 'none'];

$mOptions = new \models\Options();

$data = $mOptions->getByCondition('id_user', $_SESSION['user_id']);

$result = [];

foreach ($data as $key => $val){
    $result[] = ['options_name' => $val['name'], 'form' => json_decode($val['options'], true)];
}

echo json_encode($result);