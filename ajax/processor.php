<?php
include_once ('../core/defines.php');
include_once ('../config/config.php');
include_once ('../core/SQL.php');
include_once('../models/Base.php');
include_once ('../controllers/Base.php');
include_once('../models/Converter.php');
include_once('../controllers/Converter.php');
session_start();

$allowed_methods = ['grey', 'sepia', 'lightmult', 'rotate', 'reflect', 'rescale',
    'resize', 'watermark'];

$image['name'] = $_SESSION['image'];
$image['tmp'] = "result_" . $image['name'];
$image['path'] = USER_DATA_DIR . "images/" . $_SESSION['user_id'] . "/" . date('j-m-y') . "/";

if ($_SERVER['REQUEST_METHOD'] === "POST" && count($_POST) > 0){
    //Разделяем методы обработки изображения и их параметры
    foreach ($_POST as $key => $value){
        if (preg_match('/param_/', $key)){
            $key = preg_replace('/param_/', '', $key);
            $params[$key] = $value;   //$params[rotate_angle] = 15
        }else {
            $config[$key] = $value;   //$config[rotate] = on
        }
    }

    //Если параметров для функции несколько - записываем их в одну ячейку массива
    foreach ($params as $key => $value){
        $new_key[] = explode("_", $key);
        $params[$new_key[0][0]] .= " ".$value;
        unset($new_key);
    }

    //Удаляем пробелы в начале каждого значения
    foreach ($params as $key => $value){
        $params[$key] = substr($params[$key], 1);
    }

    foreach ($params as $key => $value){
        $params[$key] = explode(' ', $value);
    }

    //Проверяем допустимость выбраных методов
    if ($config){
        foreach ($config as $key => $value){
            if (!in_array($key, $allowed_methods)){
                unset($config[$key]);
            }
        }
    }

    $mImage = \models\Converter::instance($image['path'] . $image['name']);

    if ($config){
        foreach ($config as $method => $on){
            if ($params[$method]){
                call_user_func_array([$mImage, $method], $params[$method]);
            } else {
                $mImage->$method();
            }
        }
    }

    $mImage->render($image['path'] . $image['tmp']);
    echo json_encode("userdata/images/" . $_SESSION['user_id'] . "/" . date('j-m-y') . "/" . $image['tmp']);
}